# Mettler Toledo XPR Balance

## General notes

### Internal adjustment schedule

The XPR balance may have an internal adjustment method that is scheduled to automatically run. For long 
automation this can be an issue commands sent to the balance while it is doing an internal adjustment will error out.
To prevent this from happening, you should edit internal adjustment method, if exists, and change it to be no method.  


### Unit precision

The number of decimal places shown on the balance can be adjusted by changing the readability for the weighing 
method. Changing the readability value will also change the steps that the balance will weigh at; the best thing to 
do is to try different readability values and find what fits. This can be important, because for unstable 
environments the balance may error out if it takes too long to do a measurement in the balance is set to be very 
precision (more decimal places), but it will be faster at reporting back a value/less likely to error out if the 
balance is set to be less precise (less decimal places).  

### Recovering from a dosing error through Python

After trying to dose but getting an error through Python you need to clear and recover in order to attempt to dose again. If you just try to dose again immediately it will not work

To recover after an error has been raised
1. Cancel the current request with `MTXPRBalance.cancel_request()`
2. Re-open the session with `MTXPRBalance.open_session()`
3. Dose again and the attempt should now work

## Known issues

### Deionizer not working

There are no inidicaions when the deoionizer unit stops working. There is some regular maintainence you can do for the deionizer and it is to clean the strips, and to replace them every 1-2 years. 

You can also clean the deioinizer strip with pepsin (not sure of spelling?). 

### Updating dosing method doesn't save changes

This is an issue where after you edit a dosing method using the touchscreen (e.g. changing the doing height) and then save the method and try to use it, the old parameters get used instead of the new one. This affects whether you try to start a new dose though the touchscreen or through Python.

The fix for this is that you have to first manually cancel any running task through the touch screen, and only after that will the new update dosing parameters be used when you try to do a new dosing task. 

To cancel the current dosing task, on the terminal interface home screen:
1. near the bottom click `... more` 
2. click `cancel task`

Now the new updated dosing method parameters will be used. 

### Weighing pan on different XPR models

The weighing pan (metal grate) on the XPR10 and XPR226 models are different. This is just how it is, for our purposes we want to only use the XPR226 model because the plate design is better for our automation approach.

### Front door doesn't open

This is unlikely to be changed. If we want to have the front glass door open (like the XPE does) we need to add some actuator or remote the front door and add our own there in order to access the dosing head from the front instead of the side. 

### Draft door doesn't open/close

The reason why the draft door might not open or close is unknown, but the following error will be raised:

```python
  File "mt_xpr_balance\mt_xpr_balance.py", line 167, in close_door
    self.set_door_position(door, 0)
  File "mt_xpr_balance\mt_xpr_balance.py", line 161, in set_door_position
    self.request(self.DRAFT_SHIELDS_SERVICE, 'SetPosition', [{'DraftShieldPosition': [shield_position]}])
  File "mt_xpr_balance\mt_xpr_balance.py", line 91, in request
    raise MTXPRBalanceError(f'Error with request {service}.{method_name}({", ".join([str(a) for a in args])}): {response.ErrorMessage if hasattr(response, "ErrorMessage") else ""}')
mt_xpr_balance.mt_xpr_balance.MTXPRBalanceError: Error with request BasicHttpBinding_IDraftShieldsService.SetPosition({'DraftShieldPosition': [{'DraftShieldId': 'RightOuter', 'OpeningWidth': 0, 'OpeningSide': None}]}): 
```

This error gets thrown when there is something blocking the draft door or if there is an error with the balance 
happened. When this happens, the other draft door can still be opened/closed, and other functions of the balance 
still work. This may be an intermittent issue where only sometimes the door won't be able to open/close, or the door 
might be stuck in a non-operational state. 

When this happens a manual adjustment must be done to resolve the issue.

1. Unplug the balance
2. Take both draft doors off
3. Move the door sliders to about the middle
4. Plug in the balance

Once the balance has been plugged in the door sliders should re-home on startup. And moving forwards door control 
should work 


### Zeroing/Taring the balance

If you are using a deionizer, you should leave static detection disabled for the weighing method. Because if the balance weighing method has the ionizer and static detection active, there are issues trying to zero and tare 
the balance; the error state is `StaticDetectionFailed`. The workaround for this is to zero/tare the balance 
immediately (not wait for the balance value to stabilize), but the simplest thing is just to disable static detection. This is done in the API by:

```python
balance.zero(immediately=True)
balance.tare(immediately=True)
```

It has also been seen that even when the ionizer and static detection are not active, there may be issues with
zeroing and taring the balance if the call to do this is done too quickly after something has been placed on/removed 
off the balance. On the balance touch panel, right after placing something on the balance the screen will look like 
`---` and not show actual values. If you try to zero/tare the balance immediately while the screen is like this, you 
will get an error. In this case, you should add a few seconds wait before zeroing or taring the balance, or 
do it with immediately set to `False`:

```python
balance.zero(immediately=False)
balance.tare(immediately=False)
```

### Configure the Web Service

To remotely control the XPR the Web service needs to be activated and configured
1. Go to settings > LabX / Services
2. Set the Web Service to Active
3. Go to Web Service Configuration 
4. Configure the following
```commandline
Connection type = HTTP
port = 81
Client password = 123456
Session timeout in minutes = 120
HTTPS cettificate = Not installed
```