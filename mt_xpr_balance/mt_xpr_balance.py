import time
from typing import Optional, Any, List, Dict
import enum
import base64
import hashlib
import logging
from os import path

import suds
from suds.client import Client, SoapClient, Method, Options
import pprp
from jinja2 import Template

BASE_PATH = path.dirname(path.abspath(__file__))

logger = logging.getLogger(__name__)


class MTXPRBalanceDoors(enum.Enum):
    LEFT_OUTER = 'LeftOuter'
    RIGHT_OUTER = 'RightOuter'


class MTXPRBalance:
    BASIC_SERVICE = 'BasicHttpBinding_IBasicService'
    SESSION_SERVICE = 'BasicHttpBinding_ISessionService'
    WEIGHING_SERVICE = 'BasicHttpBinding_IWeighingService'
    WEIGHING_TASK_SERVICE = 'BasicHttpBinding_IWeighingTaskService'
    DOSING_AUTOMATION_SERVICE = 'BasicHttpBinding_IDosingAutomationService'
    NOTIFICATION_SERVICE = 'BasicHttpBinding_INotificationService'
    DRAFT_SHIELDS_SERVICE = 'BasicHttpBinding_IDraftShieldsService'

    SERVICES = [
        BASIC_SERVICE, SESSION_SERVICE, WEIGHING_SERVICE, WEIGHING_TASK_SERVICE,
        DOSING_AUTOMATION_SERVICE, NOTIFICATION_SERVICE, DRAFT_SHIELDS_SERVICE
    ]

    def __init__(self,
                 host: str,
                 port: int = 82,
                 api_path: str = 'MT/Laboratory/Balance/XprXsr/V03/MT',
                 wsdl_name: str = './wsdl/MT.Laboratory.Balance.XprXsr.V03.wsdl',
                 password: str = '123456',
                 connect: bool = True):
        self.logger = logger.getChild(self.__class__.__name__)
        self.host = host
        self.port = port
        self.api_path = api_path
        self.wsdl_path = path.join(BASE_PATH, wsdl_name)
        self.wsdl_template = self.wsdl_path + '.jinja2'
        self.password = password
        self.client: Optional[Client] = None
        self.session_id: Optional[str] = None

        if connect:
            self.connect()

    def build_wsdl_file(self):
        with open(self.wsdl_template) as template_file:
            template = Template(template_file.read())

        wsdl = template.render(host=self.host,
                               port=self.port,
                               api_path=self.api_path,
                               services=self.SERVICES)

        with open(self.wsdl_path, 'w') as wsdl_file:
            wsdl_file.write(wsdl)

    def request(self, service: str, method_name: str, args: List[Any] = [], include_session_id: bool = True, ignore_error: bool = False):
        method = getattr(self.client.service[service], method_name)
        method_args = args if not include_session_id else [self.session_id, *args]

        self.logger.debug(f'Sending request: {service}.{method_name}({", ".join([str(a) for a in args])})')

        try:
            response = method(*method_args)
        except suds.WebFault as err:
            # try reopening the session if we get a SessionIdFault error
            if 'SessionIdFault' not in err.fault.detail:
                raise err

            self.open_session()
            response = method(*method_args)

        self.logger.debug(f'Response outcome: {response.Outcome}')

        if response.Outcome != 'Success' and not ignore_error:
            error_message = response.ErrorMessage if hasattr(response, 'ErrorMessage') else str(response)  # response.ErrorState
            self.logger.error(f'Error with request: {error_message}')
            raise MTXPRBalanceRequestError(f'Error with request {service}.{method_name}({", ".join([str(a) for a in args])}): {response.ErrorMessage if hasattr(response, "ErrorMessage") else ""}')

        return response

    def connect(self):
        self.build_wsdl_file()
        self.client = Client(f'file://localhost/{self.wsdl_path}')
        self.open_session()

    def open_session(self):
        session_response = self.request(self.SESSION_SERVICE, 'OpenSession', include_session_id=False)
        try:
            self.session_id = self.decrypt_session_id(self.password, session_response.SessionId, session_response.Salt)
        except UnicodeEncodeError as error:
            raise MTXPRBalanceAuthError('Authentication error invalid password')

    def decrypt_session_id(self, password, encrypted_session_id, salt):
        decoded_session_id = base64.b64decode(encrypted_session_id)
        decoded_salt = base64.b64decode(salt)
        encoded_password = password.encode()
        key = hashlib.pbkdf2_hmac('sha1', encoded_password, decoded_salt, 1000, dklen=32)

        data_source = pprp.data_source_gen(decoded_session_id)
        decryption_gen = pprp.rijndael_decrypt_gen(key, data_source)
        session_id = pprp.decrypt_sink(decryption_gen)

        return session_id.decode()

    def tare(self, immediately: bool = False):
        response = self.request(self.WEIGHING_SERVICE, 'Tare', [str(immediately).lower()])

        return response.Outcome

    def clear_pretare_value(self):
        response = self.request(self.WEIGHING_SERVICE, 'ClearPretareValue')

        return response.Outcome

    def zero(self, immediately: bool = False):
        response = self.request(self.WEIGHING_SERVICE, 'Zero', [str(immediately).lower()])

        return response.Outcome

    def weigh(self, immediately: bool = False):
        if immediately:
            response = self.request(self.WEIGHING_SERVICE, 'GetWeight', ['Immediate'])
        else:
            response = self.request(self.WEIGHING_SERVICE, 'GetWeight')

        return response.WeightSample.NetWeight

    def cancel_request(self):
        response = self.request(self.SESSION_SERVICE, 'Cancel')
        
        return response.Outcome

    def set_door_position(self, door: MTXPRBalanceDoors, position: int):
        # We can create objects manually with a factory, or use the special dict syntax
        # shield_position = self.client.factory.create('DraftShieldPosition')
        # shield_position.DraftShieldId.value = door_name
        # shield_position.OpeningWidth = position
        # shield_position.OpeningSide = None

        # shield_positions = self.client.factory.create('ArrayOfDraftShieldPosition')
        # shield_positions.DraftShieldPosition.append(shield_position)

        # self.request(self.DRAFT_SHIELDS_SERVICE, 'SetPosition', [shield_positions])

        shield_position = {
            'DraftShieldId': door.value,
            'OpeningWidth': position,
            'OpeningSide': None
        }

        # we can use a dict to craete "arrayOf" types by using the name as the key with an array as the values
        # equivalent to:
        #         shield_positions = self.client.factory.create('ArrayOfDraftShieldPosition')
        #         shield_positions.DraftShieldPosition.append(shield_position)
        self.request(self.DRAFT_SHIELDS_SERVICE, 'SetPosition', [{'DraftShieldPosition': [shield_position]}])

    def open_door(self, door: MTXPRBalanceDoors):
        self.set_door_position(door, 100)

    def close_door(self, door: MTXPRBalanceDoors):
        self.set_door_position(door, 0)

    def get_door_position(self, door: MTXPRBalanceDoors) -> int:
        response = self.request(self.DRAFT_SHIELDS_SERVICE, 'GetPosition', [{
            'DraftShieldIdentifier': [door.value]
        }])
        door_position_info = response.DraftShieldsInformation.DraftShieldInformation[0]

        return door_position_info.OpeningWidth

    def is_door_open(self, door: MTXPRBalanceDoors) -> bool:
        position = self.get_door_position(door)

        return position > 0

    def is_dosing_head_installed(self) -> bool:
        return self.read_dosing_head() is not None

    def read_dosing_head(self) -> Optional[Dict]:
        """
        :return: information about the dosing head as a dict with keys head_id, head_type, head_type_name.

        head_id: str, e.g. "030190139144"
        head_type: str, "Powder" or "Liquid"
        head_type_name: str, head type model e.g. "QH008-BNMP"

        return example
            {
                'head_id': '030190139144',  # str
                'head_type': 'Powder',  # str
                'head_type_name': 'QH008-BNMP',  # str
                'dosing_head_info': {
                                        'substance_name': 'ISO',  # str
                                        'lot_id': '??',  # str
                                        'id_1_label': 'Var 1',  # str
                                        'id_1_value': 'Value 1',  # str
                                        'id_2_label': 'Var 2',  # str
                                        'id_2_value': 'Value 2',  # str
                                        'id_3_label': 'Var 3',  # str
                                        'tapping_while_dosing': True,  # bool
                                        'tapping_before_dosing': False,  # bool
                                        'number_of_dosages': 345  # int
                                    }
            }
        """
        try:
            response = self.request(self.DOSING_AUTOMATION_SERVICE, 'ReadDosingHead')
        except MTXPRBalanceRequestError:
            self.logger.info('No dosing head installed')
            return None

        if response.Outcome == 'Error':
            error_message = response.ErrorMessage if hasattr(response, 'ErrorMessage') else str(response)
            self.logger.error(f'Error with read dosing head request request: {error_message}')
            raise MTXPRDosingHeadError(f'Error with read dosing head request request: {error_message}')

        head_id = str(response.HeadId)
        head_type = str(response.HeadType)
        head_type_name = str(response.HeadTypeName)
        dosing_head_info = {
            'substance_name': str(response.DosingHeadInfo.SubstanceName),
            'lot_id': str(response.DosingHeadInfo.LotId),
            'id_1_label': str(response.DosingHeadInfo.Id1Label),
            'id_1_value': str(response.DosingHeadInfo.Id1Value),
            'id_2_label': str(response.DosingHeadInfo.Id2Label),
            'id_2_value': str(response.DosingHeadInfo.Id2Value),
            'id_3_label': str(response.DosingHeadInfo.Id3Label),
            'tapping_while_dosing': bool(response.DosingHeadInfo.TappingWhileDosing),
            'tapping_before_dosing': bool(response.DosingHeadInfo.TappingBeforeDosing),
            'number_of_dosages': int(response.DosingHeadInfo.NumberOfDosages),
        }

        return {
            'head_id': head_id,
            'head_type': head_type,
            'head_type_name': head_type_name,
            'dosing_head_info': dosing_head_info,
        }

    def dosing_head_substance_name(self) -> Optional[str]:
        """
        Read the installed hopper's substance_name

        :return: substance name read by balance or None if no dosing head is installed
        """
        if self.is_dosing_head_installed():
            return self.read_dosing_head()['dosing_head_info']['substance_name']
        else:
            return None

    def find_automated_dosing_method(self):
        methods = self.request(self.WEIGHING_TASK_SERVICE, 'GetListOfMethods')

        for method_description in methods.Methods.MethodDescription:
            if method_description.MethodType == 'AutomatedDosing':
                return method_description
    
    def get_dosing_method(self, name: str):
        methods = self.request(self.WEIGHING_TASK_SERVICE, 'GetListOfMethods')

        for method_description in methods.Methods.MethodDescription:
            if method_description.Name == name:
                return method_description
    
    def start_automated_dosing(self, substance_name: str, dose_amount_mg: float, lower_tolerance: float = 0.02, upper_tolerance: float = 0.02, method_name: str = None, timeout: int = 200):
        if method_name is not None:
            dosing_method = self.get_dosing_method(method_name)
        else:
            dosing_method = self.find_automated_dosing_method()
        self.request(self.WEIGHING_TASK_SERVICE, 'StartTask', [dosing_method.Name])
        actual_dose_amount_mg = None

        dosing_job = {
            'SubstanceName': substance_name,
            'VialName': 'Vial',
            'TargetWeight': {'Value': round(dose_amount_mg, 3), 'Unit': 'Milligram'},
            'LowerTolerance': {'Value': round(dose_amount_mg * lower_tolerance, 3), 'Unit': 'Milligram'},
            'UpperTolerance': {'Value': round(dose_amount_mg * upper_tolerance, 3), 'Unit': 'Milligram'},
        }

        self.request(self.DOSING_AUTOMATION_SERVICE, 'StartExecuteDosingJobListAsync', [{
            'DosingJob': [dosing_job]
        }])

        for poll_count in range(timeout):
            notifications_response = self.request(self.NOTIFICATION_SERVICE, 'GetNotifications', [500], ignore_error=True)
            for notification_type, notification in notifications_response.Notifications:
                if notification.Outcome == 'Error':
                    self.logger.warning(f'Error encountered during dosing: {notification.DosingError}')

                elif notification_type == 'DosingAutomationActionAsyncNotification':
                    action_type = notification.DosingJobActionType
                    if action_type in ('PlaceVial', 'RemoveVial', 'PlaceDosingHead', 'RemoveDosingHead'):
                        self.request(self.DOSING_AUTOMATION_SERVICE, 'ConfirmDosingJobAction', [action_type, notification.ActionItem])

                elif notification_type == 'DosingAutomationJobFinishedAsyncNotification':
                    actual_dose_amount_mg = float(notification.DosingResult.WeightSample.NetWeight.Value)
                    self.logger.info(f'Dosing automation job finished, substance: {substance_name}, requested weight: {dose_amount_mg}, actual weight: {actual_dose_amount_mg}')

                elif notification_type == 'DosingAutomationFinishedAsyncNotification':
                    self.logger.info(f'Dosing job finished, substance: {substance_name}, actual amount dosed: {actual_dose_amount_mg}mg')
                    return actual_dose_amount_mg

            time.sleep(1)

        raise MTXPRBalanceDosingError(f'Timeout while waiting for dosing job to finish, substance: {substance_name}, timeout: {timeout} seconds')

    def smart_automated_dosing(self, substance_name: str, target_dose_amount_mg: float, max_attempts: int = 20,
                               min_dosed_threshold: float = 0.9, lower_tolerance: float = 0.02,
                               upper_tolerance: float = 0.02, method_name: str = None) -> float:
        """
        This is a more robust automated dosing method. It handles the common errors in the start_automated_dosing method. This method attempts {max_attempts} doses to reach the target mass while handling any errors
        :param substance_name: name of the solid
        :param target_dose_amount_mg: the target dosing mass
        :param max_attempts: the max number of automated dosing attempts
        :param min_dosed_threshold: 0.0 - 1.0  the total amount dosed must be greater than the min_dosed_threshold multiplied by the target_dose_amount_mg for this method to consider the dose complete
        :param lower_tolerance: the automated dosing lower tolerance
        :param upper_tolerance: the automated dosing upper tolerance
        :param method_name: the automated dosing name saved on the quantos
        :return: actual mass dosed
        """
        actual_dosed_amount = 0
        remaining_dose_amount_mg = target_dose_amount_mg

        for attempt in range(max_attempts):
            self.logger.info(f'Quantos dose attempt {attempt + 1}')
            self.logger.info(f'Target solid addition weight = {target_dose_amount_mg} mg')
            self.logger.info(f'This iteration solid addition weight = {remaining_dose_amount_mg} mg')
            self.close_door(MTXPRBalanceDoors.LEFT_OUTER)
            self.close_door(MTXPRBalanceDoors.RIGHT_OUTER)
            self.tare()
            pre_dose_mass = self.weigh()
            try:
                dosed = self.start_automated_dosing(substance_name=substance_name, dose_amount_mg=remaining_dose_amount_mg, lower_tolerance=lower_tolerance, upper_tolerance=upper_tolerance, method_name=method_name)
            except MTXPRBalanceRequestError as error:
                self.logger.info(f'Quantos request error: {error}, attempting reconnection')
                self.cancel_request()
                time.sleep(1)
                self.open_session()
                time.sleep(1)
                continue
            except MTXPRBalanceDosingError as error:
                self.logger.error(f'Quantos dosing error: {error}')
                self.cancel_request()
                dosed = None

            if dosed is None:
                post_dose_mass = self.weigh()
                dosed = float(post_dose_mass.Value) - float(pre_dose_mass.Value)

            actual_dosed_amount += dosed
            remaining_dose_amount_mg -= dosed
            remaining_dose_amount_mg = round(remaining_dose_amount_mg, 3)
            self.logger.info(f'Dosed amount = {dosed} mg')
            self.logger.info(f'Total dosed amount = {actual_dosed_amount} mg')
            self.logger.info(f'Remaining solid addition weight = {remaining_dose_amount_mg} mg')

            if actual_dosed_amount > min_dosed_threshold * target_dose_amount_mg:
                break
        else:
            raise MTXPRBalanceDosingError(f'Dosing error. {max_attempts} failed attempts')

        return actual_dosed_amount


class MTXPRBalanceError(Exception):
    pass


class MTXPRBalanceAuthError(MTXPRBalanceError):
    pass


class MTXPRBalanceDosingError(MTXPRBalanceError):
    pass


class MTXPRBalanceRequestError(MTXPRBalanceError):
    pass


class MTXPRDosingHeadError(MTXPRBalanceError):
    pass

